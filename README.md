# Photo Library JavaFX application
A desktop application to store and manage your photos. All state is serialized and stored in a flat file in user space.

Application uses Java 8 with JavaFX. To use the application, just download and run the application. Use username ```admin``` to login and manage users.

### User accounts
Multiple user accounts are supported. User creation/deletion is managed by an admin account. Once a user logs in, they can view, search, create, and delete albums.
![user home](https://bytebucket.org/kpedneka/photos15/raw/2fb51960d4fadfd7005da8f1b1abef3f1eeebe88/docs/user-home.png)

### Searching
Complex searches can be performed on photos. Search by tag-value, search by date range, and specifications for matching any keywords or all keywords is supported.
![search](https://bytebucket.org/kpedneka/photos15/raw/2fb51960d4fadfd7005da8f1b1abef3f1eeebe88/docs/search.png)

### Displaying pictures
Standard format of displaying pictures in a list view.
![album content](https://bytebucket.org/kpedneka/photos15/raw/2fb51960d4fadfd7005da8f1b1abef3f1eeebe88/docs/album-content.png)