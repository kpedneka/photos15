package model;

import java.io.Serializable;

/**
 * Tag class implements {@link Serializable}. Each tag consists of a key and a value.
 * 
 * A key is a category, and the value holds some specification about the category
 * @author Kunal Pednekar(ksp101)
 *
 */
@SuppressWarnings("serial")
public class Tag implements Serializable {
	/** Type of tag */
	private String key;
	/** Value of tag */
	private String value;
	
	/**
	 * No-arg constructor for creating a new tag
	 */
	public Tag() {
		this.key = "";
		this.value = "";
	}
	
	/**
	 * 2-arg constructor for creating a new tag
	 */
	public Tag(String key, String value) {
		this.key = key;
		this.value = value;
	}
	
	/**
	 * Getter for key of tag
	 * @return - {@link String} key
	 */
	public String getKey() {
		return this.key;
	}
	
	/**
	 * Getter for value of tag
	 * @return - {@link String} value
	 */
	public String getValue() {
		return this.value;
	}
	
	/**
	 * Setter for key of tag
	 * @param key - {@link String} new key type
	 */
	public void setKey(String key) {
		this.key = key;
	}
	
	/**
	 * Setter for value of tag
	 * @param value - {@link String} new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
}
