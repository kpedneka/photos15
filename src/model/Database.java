package model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Database class for photo library. Handles persistence of data to file and getters and setters.
 * 
 * This class is designed to be a static class because it does not and should not belong to any
 * instance of any other class. It also does not have a constructor because there is no point in
 * using an instance of a Database.
 * 
 * Because the Database class is responsible for serializing Users and all their data, this class
 * can be viewed as being the top of the class hierarchy.
 * @author Kunal Pednekar (ksp101)
 *
 */
public class Database implements Serializable {
	/**
	 * Auto-generated value by Eclipse
	 */
	private static final long serialVersionUID = -7279519235078569268L;
	/** ArrayList of current users in the database */
	private static ArrayList<User> userList;
	/** directory of the data file */
	private static final String pathToDataDir = "data";
	/** file name of the data file */
	private static final String pathToDataFile ="data.dat";
	
	/**
	 * Writes the user list to the data file
	 * @throws IOException
	 */
	public static void writeToFile() throws IOException {
		ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(pathToDataDir+File.separator+pathToDataFile));
		out.writeObject(userList);
		out.close();
		for (User u : userList)
			System.out.println("adding "+u.name+" to serialization");
	}
	
	/**
	 * Reads from the data file and sets the the userList of this class
	 * @throws IOException
	 */
	public static void readFromFile() throws IOException {
		File fileChecker = new File(pathToDataDir+File.separator+pathToDataFile);
		if (fileChecker.isFile() && fileChecker.length() == 0) {
		    // file is zero length: bail out
			if (userList == null)
				userList = new ArrayList<User>();
		} else {
			System.out.println(fileChecker.length());
			ObjectInputStream in = new ObjectInputStream(new FileInputStream(pathToDataDir+File.separator+pathToDataFile));
			try {
				userList = (ArrayList<User>) in.readObject();
				if (userList == null)
					userList = new ArrayList<User>();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			in.close();	
		}
	}
	
	static void updateUser(User user) {
		User u = getUser(user.username);
		if(userList.remove(u)) {
			userList.add(user);
			try {
				writeToFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Checks to see if the user object exists in the list.
	 * Performs checks of object equality and username equality.
	 * @param user - {@link User}
	 * @return - true if found, false otherwise
	 */
	public static boolean findUser(User user) {
		for (User u : userList) {
			// if the object is the same, or the usernames match, return true
			if (u.equals(user) || u.username.equals(user.username)) return true;
		}
		return false;
	}
	
	/**
	 * Add a user object to the list, then serializes.
	 * Makes call to {@link #findUser(User)} to avoid duplicate adds.
	 * Invokes {@link #writeToFile()} after user is added to list
	 * @param user - {@link User}
	 * @return - true if successful, false otherwise
	 */
	public static boolean addUser(User user) {
		if (!findUser(user)) {
			userList.add(user);
			try {
				writeToFile();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return true;
		}
		return false;
	}
	
	/**
	 * Finds and returns user by {@link User#username}. Returns null if not found.
	 * Invokes {@link #readFromFile()} before searching for user.
	 * @param username - {@link User#username}
	 * @return - user object if found, null otherwise
	 */
	public static User getUser(String username) {
		try {
			readFromFile();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			
			
			e.printStackTrace();
		}
		if (userList.size() == 0)
			return null;
		for (User u : userList) {
			if (u.username.equals(username)) {
				return u;
			}
		}
		return null;
	}
	
	/**
	 * Deletes user from user list if the user exists. If not, does nothing.
	 * Invokes {@link #writeToFile()}.
	 * @param user - {@link model.User}
	 */
	public static void deleteUser(User user) {
		if (userList.remove(user)) {
			try {
				writeToFile();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
		}
	}
	
	/**
	 * Returns the current list of users from the database via {@link #readFromFile()}.
	 * @return {@link #userList}
	 */
	public static ArrayList<User> getUsers() {
		try {
			readFromFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (userList == null)
			userList = new ArrayList<User>();
		return userList;
	}
}
