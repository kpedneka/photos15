package model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javafx.scene.image.Image;

/**
 * Photo class implements {@link Serializable}. Holds information about a photo such as URL path to file,
 * date of last modification, set of tags, and caption.
 * Does not actually hold an image because the Image class cannot be serialized
 * @author KunalPednekar (ksp101)
 *
 */
@SuppressWarnings("serial")
public class Photo implements Serializable {
	private String imageURL;
	private long date;
	private ArrayList<Tag> tags;
	private String caption;
	
	/**
	 * Constructor for new photo object
	 * @param name - name of user
	 */
	public Photo(String imageURL, long date) {
		this.imageURL = imageURL;
		this.date = date;
		this.tags = new ArrayList<Tag>();
		this.caption="";
	}
	
	/**
	 * Gets image from imageURL
	 * @return - {@link Image} at image URL
	 */
	public Image getImage()
	{
		try {
			Image img = new Image(this.imageURL);
			return img;
		} catch (NullPointerException e) {
			System.out.println("URL is null");
			return null;
		} catch (IllegalArgumentException e) {
			System.out.println("invalid image URL");
			return null;
		}
	}
	
	/**
	 * Gets date of photo last modification in {@link String} full format
	 * @return - {@link String}, human readable version of date
	 */
	public Date getDate()
	{
		return new Date(this.date);
	}
	
	/**
	 * Gets date of photo last modification in {@link String} format "yyyy-MM-dd"
	 * @return - {@link String}, human readable version of date
	 */
	public static String convertDate(Date date)
	{
		String pattern = "MMM yyyy";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		return simpleDateFormat.format(date).toString();
	}
	
	/**
	 * Gets caption of photo
	 * @return - {@link String} caption
	 */
	public String getCaption()
	{
		if (caption == null)
			return "";
		return this.caption;
	}
	
	/**
	 * Gets all tags ({@link model.Tag}) associated with this photo
	 * @return - {@link ArrayList} list of tags ({@link model.Tag})
	 */
	public ArrayList<Tag> getTags()
	{
		return this.tags;
	}
	
	/**
	 * Updates list of tags that are associated with this photo
	 * @param tags - {@link ArrayList} list of tags ({@link model.Tag})
	 */
	public void updateTags(ArrayList<Tag> tags) {
		this.tags = new ArrayList<Tag>();
		for(Tag t : tags)
			this.tags.add(t);
	}
	
	/**
	 * Updates caption for this photo
	 * @param newCaption - {@link String} new caption
	 */
	public void updateCaption(String newCaption) {
		this.caption = newCaption;
	}
	
	/**
	 * Duplicates a photo. Used for copying a photo to another album
	 * @return - {@link Photo} object
	 */
	public Photo duplicate() {
		Photo p = new Photo(this.imageURL, this.date);
		p.updateTags(this.getTags());
		p.updateCaption(this.getCaption());
		return p;
	}
}