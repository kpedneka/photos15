package model;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * User class implements {@link Serializable}. The user holds a set of {@link Album}s
 * which in turn hold a set of {@link Photos}. Since the User class at the top of this
 * class hierarchy, all changes that must be persisted to the database happen inside 
 * this class. 
 * 
 * User class holds a directional association to the Album class. In other words, the 
 * user is aware of the albums it owns, but the album does not know about it's owner.
 * @author KunalPednekar (ksp101)
 *
 */
@SuppressWarnings("serial")
public class User implements Serializable {
	/** name of user */
	public  String name;
	/** username of user */
	public String username;
	/** albums of user */
	public ArrayList<Album> albums;
	
	
	/**
	 * Constructor for new user object
	 * @param name - name of user
	 * @param username - username of user
	 */
	public User(String name, String username) {
		this.name = name;
		this.username = username;
		this.albums = new ArrayList<Album>();
	}
	
	/**
	 * Getter for name
	 * @return {@link #name}
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * Getter for username
	 * @return {@link #username}
	 */
	public String getUsername() {
		return this.username;
	}
	
	/**
	 * Returns the album, given the name of the album
	 * @param album - {@link model.Album}
	 * @return Album
	 */
	public Album getAlbum(String album) {
		for (Album a :  this.albums) {
			if (a.getName().equals(album)) {
				return a;
			}
		}
		return null;
	}
	
	/**
	 * Adds album to the list of albums of the user
	 * @param album- {@link model.Album}
	 * @return boolean
	 */
	public boolean addAlbum(Album album) {
		if (!findAlbum(album)) {
			this.albums.add(album);
			// invoke call to database
			Database.updateUser(this);
			return true;
		}
		return false;
	}
	
	/**
	 * Adds a photo to the given album. If successful, adds, otherwise doesn't.
	 * @param album - {@link model.Album}
	 * @param photo - {@link model.Photo}
	 */
	public void addPhotoToAlbum(Album album, Photo photo) {
		Album a = getAlbum(album.getName());
		ArrayList<Photo> photoList = a.getPhotos();
		for (Photo p : photoList)
			if (p.getDate().equals(photo.getDate()))
				return;
		// only add photo if not duplicate
		a.addPhoto(photo);
		updateAlbum(a);
	}
	
	/**
	 * Updates the album in the list of albums of the user with new content
	 * @param album - {@link model.Album}
	 */
	public void updateAlbum(Album album) {
		Album toUpdate = null;
		for (Album a : this.albums) {
			if (a.getName().equals(album.getName())) {
				toUpdate = a;
			}
		}
		if (toUpdate != null) {
			this.albums.remove(toUpdate);
			addAlbum(album);
		}
	}
	
	/**
	 * Updates album name in the list of albums of the user
	 * @param album- {@link model.Album}
	 * @return boolean
	 */
	public boolean updateAlbum(Album album, String newName) {
		Album a = getAlbum(album.getName());
		if (a != null) {
			this.albums.remove(a);
			a.setName(newName);
			addAlbum(a);
			return true;
		}
		return false;
	}
	
	/**
	 * Checks if the album is present or not
	 * @param album - {@link model.Album}
	 * @return boolean
	 */
	public boolean findAlbum(Album album) {
		for (Album a: this.albums) {
			// if the object is the same, or the names match, return true
			if (a.getName().equals(album.getName())) return true;
		}
		return false;
	}
	/**
	 * Deletes album from album list if the album exists. If not, does nothing.
	 * @param user - {@link model.Album}
	 */
	public void deleteAlbum(Album album) {
		if(findAlbum(album))
			this.albums.remove(album); 
	}
	
	
	/**
	 * Returns the current list of users from the database via {@link #readFromFile()}.
	 * @return {@link #userList}
	 */
	public ArrayList<Album> getAlbums() {
		if (this.albums == null)
			this.albums = new ArrayList<Album>();
		return this.albums;
	}
	
	
	
}
