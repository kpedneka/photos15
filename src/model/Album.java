package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

/**
 * Album class implements {@link Serializable}. Holds a list of {@link Photo}s. Belongs to a user
 * @author KunalPednekar (ksp101)
 *
 */
public class Album implements Serializable {
	private String name;
	private ArrayList<Photo> photos;
	
	/**
	 * Constructor for new album object
	 * @param name - name of user
	 */
	public Album(String name) {
		this.name = name;
		this.photos = new ArrayList<Photo>();
	}
	
	/**
	 * Gets the number of photos currently in the album
	 * @return - integer value representing number of photos in album
	 */
	public int size() {
		return this.photos.size();
	}
	
	/**
	 * Gets index of the photo
	 * @param photo - {@link Photo} that belongs to this album
	 * @return - int index of photo
	 */
	public int getIndexOf(Photo photo) {
		return this.photos.indexOf(photo);
	}
	
	/**
	 * Gets the oldest photo in the album
	 * @return - {@link String} date of the oldest photo in format "yyyy-mm-dd"
	 */
	public String getOldestPhoto() {
		Date oldest = this.photos.size() == 0 ? new Date() : this.photos.get(0).getDate();
		for(Photo p : this.photos) {
			if (p.getDate().compareTo(oldest) < 0)
				oldest = p.getDate();
		}
		return Photo.convertDate(oldest);
	}
	
	/**
	 * Gets the newest photo in the album
	 * @return - {@link String} date of the newest photo in format "yyyy-mm-dd"
	 */
	public String getNewestPhoto() {
		Date newest = this.photos.size() == 0 ? new Date() : this.photos.get(0).getDate();
		for(Photo p : this.photos) {
			if (p.getDate().compareTo(newest) > 0)
				newest = p.getDate();
		}
		return Photo.convertDate(newest);
	}
	
	/**
	 * Returns all photos of the album
	 * @return - List of {@link model.Photo} objects
	 */
	public ArrayList<Photo> getPhotos() {
		return this.photos;
	}
	
	/**
	 * Gets the photo at the index specified
	 * @param index - index of photo in album
	 * @return - null if index out of bounds, otherwise {@link Photo} object
	 */
	public Photo getPhoto(int index) {
		if (index < 0 || index >= this.photos.size()) {
			return null;
		}
		return this.photos.get(index);
	}
	
	/**
	 * adds a photo to the album
	 * @param p - {@link model.Photo}
	 */
	public void addPhoto(Photo p)
	{
		photos.add(p);
	}

	public void removePhoto(Photo p) {
		this.photos.remove(p);
	}
	
	public void updatePhoto(Photo p) {
		removePhoto(p);
		addPhoto(p);
	}
	
	/**
	 * getter for the name of the album
	 * @return String
	 */
	public String getName()
	{
		return this.name;
	}
	
	/**
	 * setter for the name of album, used in to rename the album
	 * @param newName
	 */
	public void setName(String newName)
	{
		this.name = newName;
	}
}
