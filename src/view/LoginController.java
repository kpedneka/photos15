package view;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import model.Database;
import model.User;

/**
 * Login controller handles the login functions for admin and users.
 * @author Kunal Pednekar(ksp101)
 *
 */
public class LoginController implements ChangeScene {
	/** {@link TextField} element of UI allowing user to input username */
	@FXML
	TextField username;
	/** {@link Text} element of UI displaying any errors during login */
	@FXML
	Text errorMsg;
	/** {@link Button} element of UI that triggers login action */
	@FXML
	Button submit;
	
	
	/**
	 * Login function for the main application. If user is Admin, then the Admin stage is displayed.
	 * Else, the user is logged in if they exist.
	 * @param evt - ActionEvent triggered by an action in the UI
	 * @throws ClassNotFoundException
	 */
	@FXML
	protected void handleLogin(ActionEvent evt) throws ClassNotFoundException {
		String credentials = username.getText();
		
		if (credentials.equals("admin")) {
			System.out.println("Hi admin");
			loadAdminHome(evt);
		}
		else if(Database.getUser(credentials)!=null)
			loadUserHome(evt, Database.getUser(credentials));
			
		else {
			final String error = "No such user exists by that username. Please try again.";
			errorMsg.setText(error);
		}
	}
}
