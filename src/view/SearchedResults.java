package view;
/**
 * class to display searched photographs
 * @author Rachana Thanawala(rt468)
 */

import java.util.ArrayList;
import java.util.Date;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.util.Callback;
import model.Album;
import model.Photo;
import model.User;

public class SearchedResults implements ChangeScene {

	/** {@link User} that owns this album */
	private User user;
	/** ArrayList of photos from search result */
	private ArrayList<Photo> pList;
	private ObservableList<Photo> pnewList;
	@FXML
	ListView<Photo> photos;
	@FXML
	HBox hbox;
	
	/**
	 * Creates album from search results and then returns to user home
	 * @param evt - event triggered by clicking create album button
	 */
	@FXML
	private void createAlbum(ActionEvent evt) {
		Album album = new Album("Search Results "+new Date().toString());
		for (Photo p: pList)
			album.addPhoto(p);
		user.addAlbum(album);
		try {
			exit(evt);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * @param evt - event triggered by clicking new search button
	 * @throws ClassNotFoundException
	 */
	@FXML
	private void newSearch(ActionEvent evt) throws ClassNotFoundException {
		loadSearch(evt, user);
	}
	
	/**
	 * Returns navigation back to user home page
	 * @param evt - event triggered by clicking exit search button
	 * @throws ClassNotFoundException
	 */
	@FXML
	private void exit(ActionEvent evt) throws ClassNotFoundException {
		loadUserHome(evt, user);
	}
	
	/**
	 * Start function to initialize necessary variables.
	 * Display the searched photos in a listview
	 * @param user - current {@link user}
	 * @param album - current {@link Album}
	 */
	public void startSearch(ArrayList<Photo> pList, User user) {
		this.user = user;
		this.pList = pList;
		pnewList = FXCollections.observableList(pList);
		photos.setItems(pnewList);

		photos.setCellFactory(new Callback<ListView<Photo>, ListCell<Photo>>(){
			@Override
			public ListCell<Photo> call(ListView<Photo> p) {
				ImageView imageview = new ImageView();
				ListCell<Photo> cell = new ListCell<Photo>(){
					@Override
					protected void updateItem(Photo p, boolean bln) {
						super.updateItem(p, bln);
							if (p != null && p.getImage() != null) {
							// here we are setting the image and formatting it for display
							imageview.setFitHeight(50);
							imageview.setFitWidth(50);
							imageview.setPreserveRatio(true);
							imageview.setImage(p.getImage());
							setText(p.getCaption()+" - "+p.getDate());
						}
					}
				};
				cell.setGraphic(imageview);
				return cell;
			}
		});	
	}

}
