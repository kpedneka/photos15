package view;

import java.io.IOException;
import java.util.ArrayList;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import model.Album;
import model.Photo;
import model.User;

/**
 * ChangeScene interface allows application to change scenes as required.
 * The purpose of this interface is to take advantage of code reuse.
 * @author Kunal Pednekar (ksp101)
 *
 */
public interface ChangeScene {
	
	/**
	 * Loads the tag editing page
	 * @param ev - event triggered by clicking on edit tag caption
	 * @param p - the {@link model.Photo} that is being edited
	 * @param a - the {@link model.Album} that owns the picture
	 * @param u - the {@link model.User} that owns the album
	 * @throws ClassNotFoundException
	 */
	default void loadEditTags(ActionEvent ev, User u, Album a, Photo p) throws ClassNotFoundException {
		Stage primaryStage = (Stage) ((Node) ev.getSource()).getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader();   
			loader.setLocation(getClass().getResource("/view/EditTags.fxml"));
			BorderPane root = (BorderPane)loader.load();
			
			EditTagsController editTags = (EditTagsController) loader.getController();
			editTags.start(u, a, p);
			
			Scene scene = new Scene(root, 600, 400);
			primaryStage.setScene(scene);
			primaryStage.show(); 
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Loads the caption editing page
	 * @param ev - event triggered by clicking on edit photo caption
	 * @param p - the {@link model.Photo} that is being edited
	 * @param a - the {@link model.Album} that owns the picture
	 * @param u - the {@link model.User} that owns the album
	 * @throws ClassNotFoundException
	 */
	default void loadEditCaption(ActionEvent ev, User u, Album a, Photo p) throws ClassNotFoundException {
		Stage primaryStage = (Stage) ((Node) ev.getSource()).getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader();   
			loader.setLocation(getClass().getResource("/view/EditCaption.fxml"));
			AnchorPane root = (AnchorPane)loader.load();
			
			EditCaptionController editCaption = (EditCaptionController) loader.getController();
			editCaption.start(u, a, p);
			
			Scene scene = new Scene(root, 600, 400);
			primaryStage.setScene(scene);
			primaryStage.show(); 
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Loads the individual photo viewing page
	 * @param ev - event triggered by clicking on a photo in an album
	 * @param p - the {@link model.Photo} that is being displayed
	 * @param a - the {@link model.Album} that owns the picture
	 * @param u - the {@link model.User} that owns the album
	 * @param i - the index of the photo in the album
	 * @throws ClassNotFoundException
	 */
	default void loadPhoto(ActionEvent ev, User u, Album a, Photo p, int i) throws ClassNotFoundException {
		Stage primaryStage = (Stage) ((Node) ev.getSource()).getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader();   
			loader.setLocation(getClass().getResource("/view/PhotoDisplay.fxml"));
			BorderPane root = (BorderPane)loader.load();
			
			PhotoController photoDisplay = (PhotoController) loader.getController();
			photoDisplay.start(u, a, p, i);
			
			Scene scene = new Scene(root, 600, 400);
			primaryStage.setScene(scene);
			primaryStage.show(); 
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Opens the album to view all the photos in the album, allows user to perform actions on photos.
	 * @param ev - event triggered by clicking on an album 
	 * @param a - the {@link model.Album} that was clicked on
	 * @param u - the {@link model.User} that owns the album
	 * @throws ClassNotFoundException
	 */
	default void loadAlbumContent(ActionEvent ev, Album a, User u) throws ClassNotFoundException {
		Stage primaryStage = (Stage) ((Node) ev.getSource()).getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader();   
			loader.setLocation(getClass().getResource("/view/AlbumContent.fxml"));
			BorderPane root = (BorderPane)loader.load();
			
			AlbumContentController acController = (AlbumContentController) loader.getController();
			acController.start(u, a);
			
			Scene scene = new Scene(root, 600, 400);
			primaryStage.setScene(scene);
			primaryStage.show(); 
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * loads the page to add a new user to the list
	 * @param evt - event triggered by clicking on create new user button
	 * @throws ClassNotFoundException
	 */
	default void loadNewUser(ActionEvent evt) throws ClassNotFoundException {
		Stage primaryStage = (Stage) ((Node) evt.getSource()).getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader();   
			loader.setLocation(getClass().getResource("/view/NewUser.fxml"));
			GridPane root = (GridPane)loader.load();
			
			Scene scene = new Scene(root, 600, 400);
			primaryStage.setScene(scene);
			primaryStage.show(); 
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * loads the page to rename the album
	 * @param evt - the event triggered by clicking on rename album button
	 * @param user - the {@link model.User} that owns the {@link model.Album}
	 * @param obsList - observable list of {@link model.Photo}
	 * @param index - index of {@link model.Album} to be renamed
	 * @throws ClassNotFoundException
	 */
	default void loadRenameAlbum(ActionEvent evt, User user, ObservableList<Album> obsList, int index) throws ClassNotFoundException {
		Stage primaryStage = (Stage) ((Node) evt.getSource()).getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader();   
			loader.setLocation(getClass().getResource("/view/RenameAlbum.fxml"));
			GridPane root = (GridPane)loader.load();
			
			RenameAlbumController renameController = (RenameAlbumController) loader.getController();
			renameController.start(primaryStage, user, obsList, index);
			
			Scene scene = new Scene(root, 600, 400);
			primaryStage.setScene(scene);
			primaryStage.show(); 
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Loads the admin home page
	 * @param evt - the event triggered by logging in as admin
	 * @throws ClassNotFoundException
	 */
	default void loadAdminHome(ActionEvent evt) throws ClassNotFoundException {
		Stage primaryStage = (Stage) ((Node) evt.getSource()).getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader();   
			loader.setLocation(getClass().getResource("/view/AdminHome.fxml"));
			BorderPane root = (BorderPane)loader.load();

			AdminController adminController = loader.getController();
			adminController.start(primaryStage);
			
			Scene scene = new Scene(root, 600, 400);
			primaryStage.setScene(scene);
			primaryStage.show(); 
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Loads the user home page
	 * @param evt - the event triggered by logging in as a {@link model.User}
	 * @param user - the {@link model.User} that matched credentials for sign in.
	 * @throws ClassNotFoundException
	 */
	default void loadUserHome(ActionEvent evt, User user) throws ClassNotFoundException {
		Stage primaryStage = (Stage) ((Node) evt.getSource()).getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader();   
			loader.setLocation(getClass().getResource("/view/UserHome.fxml"));
			BorderPane root = (BorderPane)loader.load();

			UserController userController = loader.getController();
			userController.start(primaryStage, user);
			
			Scene scene = new Scene(root, 600, 400);
			primaryStage.setScene(scene);
			primaryStage.show(); 
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Loads a new page for searching
	 * @param evt - the event triggered by clicking on the search button
	 * @param user - current {@link User} 
	 * @throws ClassNotFoundException
	 */
	default void loadSearch(ActionEvent evt, User user) throws ClassNotFoundException {
		Stage primaryStage = (Stage) ((Node) evt.getSource()).getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader();   
			loader.setLocation(getClass().getResource("/view/Search.fxml"));
			BorderPane root = (BorderPane)loader.load();

			SearchController search = (SearchController) loader.getController();
			search.start(user);
			
			Scene scene = new Scene(root, 600, 400);
			primaryStage.setScene(scene);
			primaryStage.show(); 
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	/**
	 * Loads a new page to create a new album
	 * @param evt - the event triggered by clicking on create album button
	 * @throws ClassNotFoundException
	 */
	default void loadCreateNewAlbum(ActionEvent evt, User user, ObservableList<Album> obsList) throws ClassNotFoundException {
		Stage primaryStage = (Stage) ((Node) evt.getSource()).getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader();   
			loader.setLocation(getClass().getResource("/view/CreateNewAlbum.fxml"));
			GridPane root = (GridPane)loader.load();
			
			RenameAlbumController renameController = (RenameAlbumController) loader.getController();
			renameController.start(primaryStage, user, obsList);
			
			Scene scene = new Scene(root, 600, 400);
			primaryStage.setScene(scene);
			primaryStage.show(); 
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Logs a user or admin out of their session, returns to login page
	 * @param evt - the event triggered by clicking on logout button
	 * @throws ClassNotFoundException
	 */
	default void logout(ActionEvent evt) throws ClassNotFoundException {
		System.out.println("bye");
		BorderPane parent;
		try {
			parent = FXMLLoader.load(getClass().getResource("/view/Login.fxml"));
			Scene scene = new Scene(parent);
            Stage appStage = (Stage) ((Node) evt.getSource()).getScene().getWindow();
            appStage.setScene(scene);
            appStage.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Loads the page with the searched pictures
	 * @param ev - event triggered by clicking on edit photo caption
	 * @param p - the {@link model.Photo} that is being edited
	 * @param a - the {@link model.Album} that owns the picture
	 * @param u - the {@link model.User} that owns the album
	 * @throws ClassNotFoundException
	 */
	default void loadSearchedPhotos(ActionEvent ev, ArrayList<Photo> pList, User u) throws ClassNotFoundException {
		Stage primaryStage = (Stage) ((Node) ev.getSource()).getScene().getWindow();
		try {
			FXMLLoader loader = new FXMLLoader();   
			loader.setLocation(getClass().getResource("/view/SearchedPhotos.fxml"));
			BorderPane root = (BorderPane)loader.load();
		
			SearchedResults searchController = (SearchedResults) loader.getController();
			searchController.startSearch(pList, u);
			
			Scene scene = new Scene(root, 600, 400);
			primaryStage.setScene(scene);
			primaryStage.show(); 
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
