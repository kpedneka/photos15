package view;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.text.Text;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.Album;
import model.User;

public class RenameAlbumController implements ChangeScene {
	/** current {@link User} */
	private User user;
	/** {@link ObservableList} representing {@link Album}s of {@link #user} */
	private ObservableList<Album> obsList;
	/** Index of album that will be renamed */
	private int index;
	
	/** {@link TextField} element of UI that allow user to rename {@link Album} */
	@FXML
	TextField name;
	/** {@link Text} element of UI that displays any errors in renaming the {@link Album} */
	@FXML
	Text nameError;
	
	/**
	 * checks if new album name is present already, if yes displays an error message, else updates the album nam
	 * Reverts back to the user home page
	 * @param index - index representing the album that will be renamed
	 * @throws ClassNotFoundException
	 */
	@FXML
	private boolean validateRename(int index) {
		String newname = name.getText();
		String oldname = index == -1 ? "" : obsList.get(index).getName();
		boolean errors = false;

		final String nameEmptyErrorMsg = "Name cannot be empty";
		if(newname.trim().equals(""))
		{
			nameError.setText(nameEmptyErrorMsg);
			errors = true;
		}
		// user can rename album to same name as it already was
		if (newname.equals(oldname)) {
			return true;
		}
		//extracting old album and setting name to newname
		for(Album a : obsList) {
			if (a.getName().equals(newname)) {
				errors = true;
				break;
			}
		}
		return !errors;
	}
	
	
	/**
	 * Used to create a new album
	 * Reverts back to user home page displaying the list if added
	 * @param evt - event triggered by click on create new album button
	 * @throws ClassNotFoundException
	 */
	@FXML
	private void createAlbum(ActionEvent evt) throws ClassNotFoundException {
		String a = name.getText();
		
		//checking if textfield is empty
		final String nameEmptyErrorMsg = "Name cannot be empty";
		if(a.trim().equals("")) {
			nameError.setText(nameEmptyErrorMsg);
			return;
		}
		
		//adding a new album to the list
		System.out.println((this.user == null) ? "user is null": "user is not null"+user.username);
		if (validateRename(-1)) {
			Album album = new Album(a);
			this.user.addAlbum(album);
			System.out.println("user "+this.user.username+" has albums: "+this.user.albums.size());
		}
		loadUserHome(evt, this.user);
	}

	
	/**
	 * Rename a album from the application. Displays alert box upon error
	 * @param evt - event triggered by click on rename album button
	 * @throws ClassNotFoundException 
	 */
	@FXML
	private void renameAlbum(ActionEvent evt) throws ClassNotFoundException {
		// check if ArrayList of users is already empty
		if (obsList.isEmpty()) {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Warning Dialog");
			alert.setHeaderText("Cannot rename album");
			alert.setContentText("List is empty. No Album");
			alert.showAndWait();
			return;
		}
		// get selected album
		Album a = obsList.get(index);
		// validate new name does not already belong to album
		// and new name is not empty string
		if (validateRename(index)) {
			a.setName(name.getText().trim());
			// persist changes
			this.user.updateAlbum(a, name.getText().trim());
		}
		loadUserHome(evt, this.user);
	}
	
	/**
	 * Reverts back to the user home page
	 * @param evt - event triggered by click on cancel button
	 * @throws ClassNotFoundException
	 */
	@FXML
	private void cancel(ActionEvent evt) throws ClassNotFoundException {
		loadUserHome(evt, this.user);
	}

	/**
	 * Start function for this controller
	 * @param primaryStage - Stage that this scene will be displayed on
	 * @param user - current {@link User}
	 * @param obsList - list that represents all {@link Album}s of user
	 * @param index - index of album that needs to be renamed
	 */
	public void start(Stage primaryStage, User user, ObservableList<Album> obsList, int index) {
		this.user = user;
		this.obsList = obsList;
		this.index = index;
	}
	/**
	 * Overload method, used for initializing create new album
	 * @param primaryStage
	 * @param user
	 * @param obsList
	 */
	public void start(Stage primaryStage, User user, ObservableList<Album> obsList) {
		this.user = user;
		this.obsList = obsList;
	}
}
