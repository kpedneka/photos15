package view;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.Optional;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import model.Album;
import model.Photo;
import model.Tag;
import model.User;

public class SearchController implements ChangeScene {
	/** current {@link User} */
	private User user;
	/** {@link Album}s that belong to {@link #user} */
	private ArrayList<Album> albums;
	/** {@link ArrayList} that keeps track of all tag search parameters */
	private ArrayList<Tag> tagList;
	/** observable list used to manage parameters */
	private ObservableList<Tag> obsList;

	/** {@link TableView} element of UI to display all search parameters */
	@FXML
	TableView<Tag> table;
	/**  child of {@link #table}, represents key and value of search parameters */
	@FXML
	TableColumn<Tag, String> key, value;
	/** {@link TextField} elements of UI to get user input for adding/editing {@link Tag}s for search parameters */
	@FXML
	TextField keyForm, valueForm;
	/** {@link Button} elements of UI to confirm/cancel user addition/editing of {@link Tag}s for search parameters*/
	@FXML
	Button ok, cancel, delete;
	/** {@link DatePicker} elements of UI to select date range for search */
	@FXML
	DatePicker from, to;

	/** stores the list of new searched photos*/
	private ArrayList<Photo> pList;
	boolean check;

	@FXML
	CheckBox cb1;


	@FXML
	private void search(ActionEvent evt) {
		System.out.println("params are: \n"+tagList.toString()+"\n"+from.toString()+" "+to.toString());
	}

	/**
	 * Add a new tag to {@link #table} and {@link #tagList}.
	 * Persists changes to database via calls to methods to Data models.
	 * @param evt - event triggered by click on add tag button
	 */
	@FXML
	private void addTag(ActionEvent evt) {

		if(keyForm.getText().isEmpty()||valueForm.getText().isEmpty()) {
			ok.setText("Done");
			keyForm.setEditable(true);
			valueForm.setEditable(true);
			cancel.setDisable(false);
			delete.setDisable(false);
			return;
		}

		String error = validateTag(keyForm.getText().trim(), valueForm.getText().trim());
		if(error.equalsIgnoreCase("no error")) {
			Tag t2 = new Tag(keyForm.getText().trim(), valueForm.getText().trim());
			obsList.add(t2);
			// update ArrayList of tags
			tagList.add(t2);
			FXCollections.sort(obsList, new Comparator<Tag>() {
				@Override
				public int compare(Tag tag1, Tag tag2) {
					if(tag1.getKey().toLowerCase().compareTo(tag2.getKey().toLowerCase())==0)
						return tag1.getValue().toLowerCase().compareTo(tag2.getValue().toLowerCase());
					else
						return tag1.getKey().toLowerCase().compareTo(tag2.getKey().toLowerCase());
				}

			});
			if (obsList.size() == 1) {
				table.getSelectionModel().select(0);
			}
			else {
				int i = 0;
				for(Tag t: obsList) {
					if(t == t2) {
						table.getSelectionModel().select(i);
						break;
					}
					i++;
				}
			}
			ok.setText("Add");
			displayDetails();
			table.setItems(obsList);
			return;
		}
		else {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Warning Dialog");
			alert.setContentText(error);

			alert.showAndWait();
			return;
		}
	}

	/**
	 * Cancels add tag action to return to default state for adding tags.
	 * @param evt - event triggered by click on cancel button
	 */
	@FXML
	private void cancel(ActionEvent evt) {
		keyForm.setEditable(false);
		valueForm.setEditable(false);
		cancel.setDisable(true);
		delete.setDisable(true);
		ok.setText("Add");
		ok.setDisable(false);
		displayDetails();
	}

	/**
	 * Deletes currently selected tag in {@link #table} and {@link #tagList}.
	 * @param evt - event triggered by clicking on delete button
	 */
	@FXML
	private void deleteTag(ActionEvent evt) {
		// show dialog only if list is empty
		if (obsList.isEmpty()) {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Warning Dialog");
			alert.setHeaderText("Cannot delete tag");
			alert.setContentText("List is empty or no tag selected");
			alert.showAndWait();
			return;
		} else {
			// remove song from ArrayList and update ListView
			Tag t = table.getSelectionModel().getSelectedItem();
			int index = table.getSelectionModel().getSelectedIndex();
			if (obsList.contains(t)) {
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Delete Item");
				alert.setHeaderText("Press OK to confirm or the cross to cancel");
				String content = "Are you sure you want to delete " + t.getKey()+":"+t.getValue() + "?";
				alert.setContentText(content);
				Optional<ButtonType> result = alert.showAndWait();
				if (result.isPresent()) {
					obsList.remove(t);
					tagList.remove(t);
					if (obsList.isEmpty()) {

					} else if(index == obsList.size()-1) {
						table.getSelectionModel().select(index--);
					} else {
						table.getSelectionModel().select(index++);
					}
					displayDetails();
					table.setItems(obsList);
				}		
			}
		}
	}

	/**Checks if the input entered by the user is valid or not
	 * 
	 * @param key - {@link String } is the key of the tag. Must be present
	 * @param value - {@link String } is the value of the tag. Must be present
	 * @return appropriate message if above conditions are not met. Else return no error
	 */
	public String validateTag(String key, String value)
	{
		//checks if user has entered key and value
		if(key.isEmpty()||value.isEmpty())
			return "Key and Value cannot be left empty";
		//checks if the key has less than 20 characters
		else if ((!key.trim().isEmpty()) && (key.trim().length() >= 20))
			return "Key must be less than 20 characters long.";
		//compares the title and artist to ensure no repetitions exist
		else if(!uniqueTag(key, value))
			return "Tag already exists";
		else
			return "no error";
	}

	/** Check for repetitions by comparing key and value. Strings are converted to lower case to make the comparison case-insensitive
	 * 
	 * @param key - {@link String } is the key of the tag
	 * @param value - {@link String } is the value of the tag
	 * @return false if the tag already exists and true if the tag does not exist
	 */
	public boolean uniqueTag(String key, String value) {
		for (Tag t : obsList) {
			if (t.getKey().toLowerCase().equals(key.toLowerCase()) && t.getValue().toLowerCase().equals(value.toLowerCase()))
				return false;
		}
		return true;
	}

	/**
	 * updates TextFields at the bottom of the BorderPane view.
	 */
	private void displayDetails() {
		keyForm.setText("");
		valueForm.setText("");
		keyForm.setPromptText("Key");
		valueForm.setPromptText("Value");

		keyForm.setEditable(false);
		valueForm.setEditable(false);
		cancel.setDisable(true);
		if (tagList.isEmpty())
			delete.setDisable(true);
		else
			delete.setDisable(false);
		return;
	}

	/**
	 * Returns to user home page
	 * @param evt - event triggered by click on go back button
	 * @throws ClassNotFoundException 
	 */
	@FXML
	private void goBack(ActionEvent evt) throws ClassNotFoundException {
		loadUserHome(evt, user);
	}

	/**
	 * Start function for search page controller
	 * @param user - current {@link User}
	 */
	public void start(User user) {
		this.user = user;
		this.albums = user.albums;
		// set formatting for cell
		key.setCellValueFactory(new PropertyValueFactory<>("key"));
		value.setCellValueFactory(new PropertyValueFactory<>("value"));

		keyForm.setEditable(false);
		valueForm.setEditable(false);
		cancel.setDisable(true);
		delete.setDisable(true);

		tagList = new ArrayList<Tag>();

		obsList = FXCollections.observableArrayList();
		table.setItems(obsList);

		if (!obsList.isEmpty()) {
			table.getSelectionModel().select(0);
			delete.setDisable(false);
		}

		cb1.selectedProperty().addListener(new ChangeListener<Boolean>() {
			public void changed(ObservableValue<? extends Boolean> ov,
					Boolean old_val, Boolean new_val) {
				check = new_val;
				System.out.println(check);
			}
		});
	}

	/**
	 * Function to search photos by date and tag ket and tage value
	 * @param evt
	 * @throws ClassNotFoundException
	 */
	@FXML
	public void Search(ActionEvent evt)throws ClassNotFoundException
	{

		if(check)
			searchAND(evt);
		else
			searchOR(evt);
	}

	/**
	 * search by date and by either one of the tags
	 * @param evt
	 * @throws ClassNotFoundException
	 */
	public void searchOR(ActionEvent evt) throws ClassNotFoundException
	{
		pList = new ArrayList<Photo>();
		//search by date
		if(to.getValue()!=null && from.getValue()!=null)
		{
			Date sdate = java.sql.Date.valueOf(from.getValue());
			Date edate = java.sql.Date.valueOf(to.getValue());

			for(Album a:albums)
			{
				for(Photo p: a.getPhotos())
				{
					if(p.getDate().after(sdate) && p.getDate().before(edate))
					{
						pList.add(p);
					}
				}
			}
		}

		//search by tags - OR
		for(Tag t1:tagList)
		{
			for(Album a:albums)
			{
				for(Photo p: a.getPhotos())
				{
					for(Tag t: p.getTags())
					{
						if(t1.getKey().equals(t.getKey())&&t1.getValue().equals(t.getValue()))
						{
							if(!pList.contains(p))
								pList.add(p);
						}
					}
				}
			}
		}
		//call page to display searched photos
		loadSearchedPhotos(evt, pList, user);

	}

	/**
	 * Function to search photos by date and tag key and tag value
	 * @param evt
	 * @throws ClassNotFoundException
	 */
	@FXML
	public void searchAND(ActionEvent evt)throws ClassNotFoundException
	{
		pList = new ArrayList<Photo>();
		//search by tags - AND
		int count =0;
		int size = tagList.size();
		for (Album a : albums) {
			for (Photo p : a.getPhotos()) {
				count =0;
				for(Tag t: tagList) {
					for(Tag t1: p.getTags()) {
						if(t1.getKey().equals(t.getKey())&&t1.getValue().equals(t.getValue()))
							count++;
					}
				}
				if (count==size) {
					if(!pList.contains(p))
						pList.add(p);
				} else
					System.out.println("failed");
			}
		}

		// search by date
		if (to.getValue() != null && from.getValue() != null) {
			Date sdate = java.sql.Date.valueOf(from.getValue());
			Date edate = java.sql.Date.valueOf(to.getValue());

			for (Photo p : pList) {
				if (p.getDate().after(sdate) && p.getDate().before(edate))
					pList.add(p);
			}
		}
		//call page to display searched photos
		loadSearchedPhotos(evt, pList, user);
	}
}






