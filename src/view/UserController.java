package view;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Optional;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Callback;
import model.Album;
import model.Database;
import model.User;



public class UserController implements ChangeScene {
	/** {@link ListView} element of UI to display all {@link Album}s that belong to {@link #user} */
	@FXML
	ListView<Album> listView; 
	/** {@link Button} elements of UI that allow user to trigger various actions */
	@FXML
	Button createAlbum, searchAlbum, logout, cancel, create, renamedone, delete, rename;
	/** {@link TextField} element of UI that allows user to rename album */
	@FXML
	TextField albumName;
	/** {@link Text} elements of UI that display information */
	@FXML
	Text nameError, oldName;


	private ObservableList<Album> obsList;
	/** user that is currently signed in */
	private User user;
	
	/**
	 * loads a new page to create a new album
	 * @param evt - event triggered by click on rename album button in user home
	 * @throws ClassNotFoundException
	 */
	@FXML
	public void createNewAlbum(ActionEvent evt) throws ClassNotFoundException
	{
		loadCreateNewAlbum(evt, this.user, this.obsList);
	}
	
	
	/**
	 * loads a new page to search for pictures
	 * @param evt - event triggered by click on search album button
	 * @throws ClassNotFoundException
	 */
	@FXML
	private void searchAlbum(ActionEvent evt) throws ClassNotFoundException
	{
		loadSearch(evt, this.user);
	}

	/**
	 * Used to create a new album
	 * Reverts back to user home page displaying the list if added
	 * @param evt - event triggered by click on create new album button
	 * @throws ClassNotFoundException
	 */
	@FXML
	private void createAlbum(ActionEvent evt) throws ClassNotFoundException {
		String a = albumName.getText();
		boolean errors = false;
		
		//checking if textfiels is empty
		final String nameEmptyErrorMsg = "Name cannot be empty";
		if(a.trim().equals(""))
		{
			nameError.setText(nameEmptyErrorMsg);
			errors = true;
		}
		
		//adding a new album to the list
		System.out.println((this.user == null) ? "user is null": "user is not null"+user.username);
		if (this.user.getAlbum(a) == null && !errors) {
			Album album = new Album(a);
			this.user.addAlbum(album);
			loadUserHome(evt, this.user);
		}
		//error msg for duplicate album name
		else if (user.getAlbum(a) != null && !errors) {
			final String usernameTakenErrorMsg = "Album already exists";
			nameError.setText(usernameTakenErrorMsg);
			errors = true;
		}
	}

	/**
	 * Logout function for user.
	 * @param evt - event triggered by mouse click on logout button
	 * @throws ClassNotFoundException
	 */
	@FXML
	private void handleLogout(ActionEvent evt) throws ClassNotFoundException {
		System.out.println("bye");
		logout(evt);
	}

	/**
	 * Delete a album from the application. Displays alert box upon error
	 * @param evt - event triggered by click on delete album button
	 */
	@FXML
	private void deleteAlbum(ActionEvent evt) {
		// check if ArrayList of users is already empty
		if (obsList.isEmpty()) {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Warning Dialog");
			alert.setHeaderText("Cannot delete Album");
			alert.setContentText("List is empty. No album");
			alert.showAndWait();
			return;
		}
		Album a = listView.getSelectionModel().getSelectedItem();
		int index = listView.getSelectionModel().getSelectedIndex();
		if (obsList.contains(a)) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Delete Album");
			alert.setHeaderText("Press OK to confirm or the cross to cancel");

			String content = "Are you sure you want to delete " + a.getName() + "?";

			alert.setContentText(content);

			Optional<ButtonType> result = alert.showAndWait();
			if (result.isPresent()) {
				obsList.remove(a);
				user.deleteAlbum(a);
				if(index == obsList.size()-1)
					listView.getSelectionModel().select(index--);
				else
					listView.getSelectionModel().select(index++);
				listView.setItems(obsList);
			}		
		}
	}


	/**
	 * Rename a album from the application. Displays alert box upon error
	 * calls a new page to rename the album
	 * @param evt - event triggered by click on rename album button
	 * @throws ClassNotFoundException 
	 */
	@FXML
	private void renameAlbum(ActionEvent evt) throws ClassNotFoundException {
		// check if ArrayList of users is already empty
		if (obsList.isEmpty()) {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Warning Dialog");
			alert.setHeaderText("Cannot rename album");
			alert.setContentText("List is empty. No Album");
			alert.showAndWait();
			return;
		}

		Album a = listView.getSelectionModel().getSelectedItem();
		int index = listView.getSelectionModel().getSelectedIndex();
		if (obsList.contains(a)) {
			loadRenameAlbum(evt, this.user, obsList, index);
			
		}		
	}

	
	/**
	 * Start method for the controller
	 * @param mainStage - stage where this scene will be displayed
	 */
	public void start(Stage mainStage, User user) {
		this.user = user;
		ArrayList<Album> list = this.user.getAlbums();
		obsList = FXCollections.observableArrayList(list);     
		FXCollections.sort(obsList, new Comparator<Album>() {
			@Override
			public int compare(Album album1, Album album2) {
				return album1.getName().toLowerCase().compareTo(album2.getName().toLowerCase());
			}
		});

		listView.setItems(obsList);

		listView.setCellFactory(new Callback<ListView<Album>, ListCell<Album>>(){
			@Override
			public ListCell<Album> call(ListView<Album> p) {
				ListCell<Album> cell = new ListCell<Album>(){
					@Override
					protected void updateItem(Album a, boolean bln) {
						super.updateItem(a, bln);
						if (a != null) {
							//displays name of the album
							setText(a.getName()+" - ("+a.size()+") "+a.getOldestPhoto()+"-"+a.getNewestPhoto());
				            setOnMouseClicked(ev -> {
				            		if(ev.getClickCount() == 2) {
				            			try {
										loadAlbumContent(new ActionEvent(ev.getSource(), ev.getTarget()), a, user);
									} catch (ClassNotFoundException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}	
				            		}
							});
						}
						else if (a == null) {
							setText(null);
				            setOnMouseClicked(null);
						}
					}
				};
				return cell;
			}
		});
		
		// preselect first element
		if (!obsList.isEmpty())
			listView.getSelectionModel().select(0);
	}
}
