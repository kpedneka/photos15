package view;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Callback;
import model.Album;
import model.Photo;
import model.User;

public class AlbumContentController implements ChangeScene {
	/** {@link User} that owns this album */
	private User user;
	/** {@link Album} that contains this photo */
	private Album album;
	/** {@link ObservableList} that represents all  {@link Photo}s in  {@link Album} */
	private ObservableList<Photo> obsList;
	/** {@link ListView} that represents all the photos in the current album */
	@FXML
	ListView<Photo> photos;
	/** {@link Button} element in UI for album content screen*/
	@FXML
	Button goBack;
	/** {@link HBox} element in UI for album content screen */
	@FXML
	HBox hbox;
	
	/**
	 * Returns back to the user home page.
	 * @param evt - event triggered by click on go back button in album content page
	 * @throws ClassNotFoundException
	 */
	@FXML
	private void goBack(ActionEvent evt) throws ClassNotFoundException {
		loadUserHome(evt, this.user);
	}
	
	/**
	 * Returns from selection state to default state of album content
	 * @param evt - event triggered by click on done button
	 * @throws ClassNotFoundException
	 */
	@FXML
	private void done(ActionEvent evt) throws ClassNotFoundException {
		loadAlbumContent(evt, this.album, this.user);
	}
	
	/**
	 * Helper function for selection. Deletes multiple photos from an album
	 * @param deleteThese - {@link ArrayList} of {@link Photo}s in {@link Album}
	 */
	private void delete(ArrayList<Photo> deleteThese) {
		for(Photo p : deleteThese) {
			album.removePhoto(p);
		}
		user.updateAlbum(album);
		obsList = FXCollections.observableArrayList(album.getPhotos());
		this.photos.setItems(obsList);
	}
	
	/**
	 * Helper function for selection. Copies photos from an album to another
	 * @param copyThese - {@link ArrayList} of {@link Photo}s in {@link Album}
	 */
	private void copy(ArrayList<Photo> copyThese, String albumName) {
		Album a = user.getAlbum(albumName);
		if (a == null)
			return;
		for(Photo p : copyThese) {
			a.addPhoto(p.duplicate());
		}
		user.updateAlbum(a);
	}
	
	/**
	 * Helper function for selection. Moves photos from an album to another
	 * @param moveThese - {@link ArrayList} of {@link Photo}s in {@link Album}
	 */
	private void move(ArrayList<Photo> moveThese, String albumName) {
		Album a = user.getAlbum(albumName);
		if (a == null)
			return;
		for(Photo p : moveThese) {
			album.removePhoto(p);
			a.addPhoto(p);
		}
		// persist changes
		user.updateAlbum(a);
		user.updateAlbum(album);
		// update list of photos for current album
		obsList = FXCollections.observableArrayList(album.getPhotos());
		this.photos.setItems(obsList);
	}
	
	/**
	 * Upload function supports uploading a single image at a time. Format of image must be
	 * .jpg or .png. Creates a new Image object and adds it to the user's album.
	 * @param evt - event triggered by click on upload button in album content page
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	@FXML
	private void upload(ActionEvent evt) throws ClassNotFoundException, IOException {
		
		FileChooser fileChooser = new FileChooser();
		
		FileChooser.ExtensionFilter extFilterJPG = new FileChooser.ExtensionFilter("Image files (*.jpg, *.png)", "*.JPG", "*.PNG");
        fileChooser.getExtensionFilters().addAll(extFilterJPG);
		
		fileChooser.setTitle("Upload Photo");
		Stage app_stage = (Stage)((Node)evt.getSource()).getScene().getWindow();
		
		List<File> list = fileChooser.showOpenMultipleDialog(app_stage);
		if (list != null) {
			for (File file : list) {
				String imageURL = file.toURI().toString();
				Photo photo = new Photo(imageURL, file.lastModified());
				this.user.addPhotoToAlbum(this.album, photo);
			}
        }
		
        
        loadAlbumContent(evt, this.album, this.user);
	}
	
	/**
	 * Selection view. Allows user to select multiple images and then
	 * perform either move, copy, delete actions on the selected images.
	 * @param user - current {@link model.User}
	 * @param album - current {@link model.Album}
	 */
	@FXML
	private void selection(ActionEvent evt) {
		obsList = FXCollections.observableList(album.getPhotos());
		
		photos.setItems(obsList);
		
		ArrayList<Photo> indexes = new ArrayList<Photo>();

		photos.setCellFactory(new Callback<ListView<Photo>, ListCell<Photo>>(){
			@Override
			public ListCell<Photo> call(ListView<Photo> p) {
				StackPane sp = new StackPane();
				ImageView imageview = new ImageView();
				CheckBox cb = new CheckBox();
				ListCell<Photo> cell = new ListCell<Photo>(){
					@Override
					protected void updateItem(Photo p, boolean bln) {
						super.updateItem(p, bln);
						if (p != null && p.getImage() != null) {
							// here we are setting the image and formatting it for display
							imageview.setFitHeight(50);
							imageview.setFitWidth(50);
							imageview.setPreserveRatio(true);
							imageview.setImage(p.getImage());
							cb.selectedProperty().addListener(new ChangeListener<Boolean>() {
						        public void changed(ObservableValue<? extends Boolean> ov,
						            Boolean old_val, Boolean new_val) {
						                if (new_val) {
						                		imageview.setOpacity(0.65);
							                indexes.add(obsList.get(obsList.indexOf(p)));
						                } else {
						                		imageview.setOpacity(0.65);
						                		indexes.remove(obsList.get(obsList.indexOf(p)));
						                }
						        }
						    });
							sp.getChildren().addAll(imageview, cb);
							StackPane.setAlignment(imageview, Pos.CENTER_LEFT);
							StackPane.setAlignment(cb, Pos.TOP_LEFT);
							setGraphic(sp);
							setText(p.getCaption()+" - "+p.getDate());
						}
					}
				};
				return cell;
				
			}
		});
		
		// button to go back to original state of album content
		Button done = new Button("Done");
		done.setOnAction(new EventHandler<ActionEvent>() {
            @Override 
            public void handle(ActionEvent e) {
                try {
					done(e);
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
            }
        });
		// button to move photos to another album
		Button move = new Button("Move");
		move.setOnAction(new EventHandler<ActionEvent>() {
            @Override 
            public void handle(ActionEvent e) {
                System.out.println("Hi I should move photos to another album");
                List<String> choices = new ArrayList<>();
                // add album names as choices
                for(Album a : user.albums) {
                		if (!a.getName().equals(album.getName()))
                			choices.add(a.getName());
                }
                if (choices.size() > 0) {
                		ChoiceDialog<String> dialog = new ChoiceDialog<String>(choices.get(0), choices);
                		dialog.setTitle("Move Photos");
                		dialog.setHeaderText("Move photos to another album");
                		dialog.setContentText("Choose destination album:");
                		Optional<String> result = dialog.showAndWait();
                		result.ifPresent(choice -> move(indexes, choice));	
                } else {
                		Alert alert = new Alert(AlertType.WARNING);
                		alert.setTitle("Warning");
	                	alert.setHeaderText("Operation cannot execute");
	                	alert.setContentText("There are no other albums to move this set of photos to. Create a new album, then try again.");

	                	alert.showAndWait();
                }
            }
        });
		// button to copy photos to another album
		Button copy = new Button("Copy");
		copy.setOnAction(new EventHandler<ActionEvent>() {
            @Override 
            public void handle(ActionEvent e) {
                System.out.println("Hi I should copy photos to another album");
                List<String> choices = new ArrayList<>();
                // add album names as choices
                for(Album a : user.albums) {
                		if (!a.getName().equals(album.getName()))
                			choices.add(a.getName());
                }
                if (choices.size() > 0) {
                		ChoiceDialog<String> dialog = new ChoiceDialog<String>(choices.get(0), choices);
                		dialog.setTitle("Copy Photos");
                		dialog.setHeaderText("Copy photos to another album");
                		dialog.setContentText("Choose destination album:");
                		Optional<String> result = dialog.showAndWait();
                		result.ifPresent(choice -> copy(indexes, choice));	
                } else {
                		Alert alert = new Alert(AlertType.WARNING);
                		alert.setTitle("Warning");
	                	alert.setHeaderText("Operation cannot execute");
	                	alert.setContentText("There are no other albums to copy this set of photos to. Create a new album, then try again.");
	
	                	alert.showAndWait();
                }
            }
        });
		// button to delete photos
		Button delete = new Button("Delete");
		delete.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                System.out.println("Hi I should delete stuff");
                delete(indexes);
                // persist changes
                user.updateAlbum(album);
                try {
					loadAlbumContent(e, album, user);
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
            }
        });
		// add buttons to hbox, set spacing
		hbox.getChildren().clear();
		hbox.setAlignment(Pos.CENTER);
		hbox.setSpacing(10);
		hbox.getChildren().addAll(done, move, copy, delete);
	}
	
	/**
	 * Start function to initialize necessary variables.
	 * @param user - current {@link user}
	 * @param album - current {@link Album}
	 */
	public void start(User user, Album album) {
		this.album = album;
		this.user = user;
		obsList = FXCollections.observableList(album.getPhotos());
		
		// button to go back to user home
		Button goBack = new Button("Go Back");
		goBack.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                try {
					loadUserHome(e, user);
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
            }
        });
		// button to upload photos
		Button uploadPhotos = new Button("Upload");
		uploadPhotos.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                try {
					upload(e);
				} catch (ClassNotFoundException | IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
            }
        });
		// button to multi-select photos for actions
		Button selection = new Button("Select...");
		selection.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                selection(e);
            }
        });
		// add buttons to hbox, set spacing
		hbox.setAlignment(Pos.CENTER);
		hbox.setSpacing(10);
		hbox.getChildren().addAll(goBack, uploadPhotos, selection);
		// set ListView
		photos.setItems(obsList);

		photos.setCellFactory(new Callback<ListView<Photo>, ListCell<Photo>>(){
			@Override
			public ListCell<Photo> call(ListView<Photo> p) {
				ImageView imageview = new ImageView();
				ListCell<Photo> cell = new ListCell<Photo>(){
					@Override
					protected void updateItem(Photo p, boolean bln) {
						super.updateItem(p, bln);
						// mouse click for going to individual photo view
						setOnMouseClicked(ev -> {
							if (ev.getClickCount() == 2) {
								try {
									loadPhoto(new ActionEvent(ev.getSource(), ev.getTarget()), user, album, p, album.getIndexOf(p));
								} catch (ClassNotFoundException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}	
							}
						});
						if (p != null && p.getImage() != null) {
							// here we are setting the image and formatting it for display
							imageview.setFitHeight(50);
							imageview.setFitWidth(50);
							imageview.setPreserveRatio(true);
							imageview.setImage(p.getImage());
							setText(p.getCaption()+" - "+p.getDate());
						}
					}
				};
				cell.setGraphic(imageview);
				return cell;
			}
		});	
	}
}
