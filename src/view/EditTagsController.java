package view;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Optional;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import model.Album;
import model.Photo;
import model.Tag;
import model.User;

public class EditTagsController implements ChangeScene {
	/** photo being displayed */
	private Photo photo;
	/** user who owns the photo that is being displayed individually */
	private User user;
	/** album that individual photo is in */
	private Album album;
	/** {@link ArrayList} that keeps track of current {@link Tag}s in photos */
	private ArrayList<Tag> tagList;
	/** observable list used to manage {@link #table} */
	private ObservableList<Tag> obsList;
	
	/** {@link TableView} element of UI to display all tags */
	@FXML
	TableView<Tag> table;
	/**  child of {@link #table}, represents information for key and value of {@link Tag}*/
	@FXML
	TableColumn key, value;
	/** {@link TextField} elements of UI to get user input for adding/editing {@link Tag}s */
	@FXML
	TextField keyForm, valueForm;
	/** {@link Button} elements of UI to confirm/cancel user addition/editing of {@link Tag}s */
	@FXML
	Button ok, cancel;
	
	/**
	 * Goes back to individual photo view
	 * @param evt - event triggered by clicking done button
	 * @throws ClassNotFoundException 
	 */
	@FXML
	private void done(ActionEvent evt) throws ClassNotFoundException {
		loadPhoto(evt, user, album, photo, album.getIndexOf(photo));
	}
	
	/**
	 * Add a new tag to {@link #table} and {@link #tagList}.
	 * Persists changes to database via calls to methods to Data models.
	 * @param evt - event triggered by click on add tag button
	 */
	@FXML
	private void addTag(ActionEvent evt) {
		keyForm.setText("");
		valueForm.setText("");
		keyForm.setPromptText("Key");
		valueForm.setPromptText("Value");
		keyForm.setEditable(true);
		valueForm.setEditable(true);

		cancel.setDisable(false);
		ok.setDisable(false);
		
		ok.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {

				String error = validateTag(keyForm.getText().trim(), valueForm.getText().trim());
				
				if(error.equalsIgnoreCase("no error")) {
					Tag t2 = new Tag(keyForm.getText().trim(), valueForm.getText().trim());
					obsList.add(t2);
					// update ArrayList of tags
					tagList.add(t2);
					FXCollections.sort(obsList, new Comparator<Tag>() {
						@Override
						public int compare(Tag tag1, Tag tag2) {
							if(tag1.getKey().toLowerCase().compareTo(tag2.getKey().toLowerCase())==0)
								return tag1.getValue().toLowerCase().compareTo(tag2.getValue().toLowerCase());
							else
							return tag1.getKey().toLowerCase().compareTo(tag2.getKey().toLowerCase());
						}

					});
					if (obsList.size() == 1) {
						table.getSelectionModel().select(0);
					}
					else {
						int i = 0;
						for(Tag t: obsList) {
							if(t == t2) {
								table.getSelectionModel().select(i);
								break;
							}
							i++;
						}
					}
					displayDetails();
					// persist changes
					photo.updateTags(tagList);
					album.updatePhoto(photo);
					user.updateAlbum(album);
					System.out.println("user has been updated");
				}
				else {
					Alert alert = new Alert(AlertType.WARNING);
					alert.setTitle("Warning Dialog");
					alert.setContentText(error);

					alert.showAndWait();
				}

			}
		});
		
		cancel.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				displayDetails();
			}
		});
	}
	
	/**
	 * Edits a tag in {@link #table} and {@link #tagList}.
	 * Persists changes to database via calls to methods to Data models.
	 * @param evt - event triggered by click on edit tag button
	 */
	public void editTag(ActionEvent evt) {
		if (obsList.isEmpty())
		{
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Warning Dialog");
			alert.setContentText("List is empty. No tag to edit");
			alert.showAndWait();
			displayDetails();
			return;
		}
		
		Tag t1 = table.getSelectionModel().getSelectedItem();
		keyForm.setText(t1.getKey());
		valueForm.setText(t1.getValue());
		keyForm.setEditable(true);
		valueForm.setEditable(true);

		cancel.setDisable(false);
		ok.setDisable(false);
		
		ok.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				String error;
				Tag t2 = table.getSelectionModel().getSelectedItem();
				int index = tagList.indexOf(t2);
				if(keyForm.getText().trim().toLowerCase().equals(t2.getKey().toLowerCase()) && valueForm.getText().trim().toLowerCase().equals(t2.getValue().toLowerCase()))
					error = "no changes made";
				else	
					error= validateTag(keyForm.getText().toLowerCase().trim(), valueForm.getText().toLowerCase().trim());
				
				if(error.equalsIgnoreCase("no error")) {
					t1.setKey(keyForm.getText().toLowerCase().trim());
					t1.setValue(valueForm.getText().toLowerCase().trim());
					// update ArrayList of tags
					if (index != -1) {
						tagList.remove(index);
						tagList.add(index, t2);
					}
					obsList.remove(index);
					obsList.add(index, t2);
					FXCollections.sort(obsList, new Comparator<Tag>() {
						@Override
						public int compare(Tag tag1, Tag tag2) {
							if(tag1.getKey().toLowerCase().compareTo(tag2.getKey().toLowerCase())==0)
								return tag1.getValue().toLowerCase().compareTo(tag2.getValue().toLowerCase());
							else
							return tag1.getKey().toLowerCase().compareTo(tag2.getKey().toLowerCase());
						}

					});
					if (obsList.size() == 1) {
						table.getSelectionModel().select(0);
					}
					else {
						int i = 0;
						for(Tag t: obsList) {
							if(t == t2) {
								table.getSelectionModel().select(i);
								break;
							}
							i++;
						}
					}
					displayDetails();
					// persist changes
					photo.updateTags(tagList);
					album.updatePhoto(photo);
					user.updateAlbum(album);
					System.out.println("user has been updated");
				}
				else {
					Alert alert = new Alert(AlertType.WARNING);
					alert.setTitle("Warning Dialog");
					alert.setContentText(error);
					alert.showAndWait();
				}
			}
		});
		
		cancel.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				displayDetails();
			}
		});
	}
	
	/**
	 * Deletes currently selected tag in {@link #table} and {@link #tagList}.
	 * @param evt - event triggered by clicking on delete button
	 */
	@FXML
	private void deleteTag(ActionEvent evt) {
		// show dialog only if list is empty
		if (obsList.isEmpty()) {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Warning Dialog");
			alert.setHeaderText("Cannot delete tag");
			alert.setContentText("List is empty or no tag selected");
			alert.showAndWait();
			return;
		} else {
			// remove song from ArrayList and update ListView
			Tag t = table.getSelectionModel().getSelectedItem();
			int index = table.getSelectionModel().getSelectedIndex();
			if (obsList.contains(t)) {
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Delete Item");
				alert.setHeaderText("Press OK to confirm or the cross to cancel");
				String content = "Are you sure you want to delete " + t.getKey()+":"+t.getValue() + "?";
				alert.setContentText(content);
				Optional<ButtonType> result = alert.showAndWait();
				if (result.isPresent()) {
					obsList.remove(t);
					tagList.remove(t);
					if (obsList.isEmpty()) {
									
					} else if(index == obsList.size()-1) {
						table.getSelectionModel().select(index--);
					} else {
						table.getSelectionModel().select(index++);
					}
					displayDetails();
					table.setItems(obsList);
				}		
			}
		}
		// persist changes
		photo.updateTags(tagList);
		album.updatePhoto(photo);
		user.updateAlbum(album);
	}
	
	/**Checks if the input entered by the user is valid or not
	 * 
	 * @param key - {@link String } is the key of the tag. Must be present
	 * @param value - {@link String } is the value of the tag. Must be present
	 * @return appropriate message if above conditions are not met. Else return no error
	 */
	public String validateTag(String key, String value)
	{
		//checks if user has entered key and value
		if(key.isEmpty()||value.isEmpty())
			return "Key and Value cannot be left empty";
		//checks if the key has less than 20 characters
		else if ((!key.trim().isEmpty()) && (key.trim().length() >= 20))
			return "Key must be less than 20 characters long.";
		//compares the title and artist to ensure no repetitions exist
		else if(!uniqueTag(key, value))
			return "Tag already exists";
		else
			return "no error";
	}
	
	/** Check for repetitions by comparing key and value. Strings are converted to lower case to make the comparison case-insensitive
	 * 
	 * @param key - {@link String } is the key of the tag
	 * @param value - {@link String } is the value of the tag
	 * @return false if the tag already exists and true if the tag does not exist
	 */
	public boolean uniqueTag(String key, String value) {
		for (Tag t : obsList) {
			if (t.getKey().toLowerCase().equals(key.toLowerCase()) && t.getValue().toLowerCase().equals(value.toLowerCase()))
				return false;
		}
		return true;
	}
	
	/**
	 * updates TextFields at the bottom of the BorderPane view.
	 */
	private void displayDetails() {
		if (table.getSelectionModel().getSelectedIndex() < 0) {
			keyForm.setText("");
			valueForm.setText("");
			keyForm.setPromptText("Key");
			valueForm.setPromptText("Value");
			
			keyForm.setEditable(false);
			valueForm.setEditable(false);
			ok.setDisable(true);
			cancel.setDisable(true);
			return;
		}
			
		Tag t = table.getSelectionModel().getSelectedItem();
		// update the VBox to show the details of the song that is selected
		keyForm.setText(t.getKey());
		valueForm.setText(t.getValue());
		
		keyForm.setEditable(false);
		valueForm.setEditable(false);
		ok.setDisable(true);
		cancel.setDisable(true);
	}
	
	/**
	 * Start function for Edit tags page
	 * @param user
	 * @param album
	 * @param photo
	 */
	public void start(User user, Album album, Photo photo) {
		this.user = user;
		this.album = album;
		this.photo = photo;
		tagList = photo.getTags();
		obsList = FXCollections.observableArrayList(photo.getTags());
		// set formatting for cell
		key.setCellValueFactory(new PropertyValueFactory<>("key"));
		value.setCellValueFactory(new PropertyValueFactory<>("value"));
		table.setItems(obsList);
		
		if (!obsList.isEmpty())
			table.getSelectionModel().select(0);
		
		displayDetails();
	}
}
