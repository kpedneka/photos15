package view;

import java.util.ArrayList;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import model.Album;
import model.Photo;
import model.Tag;
import model.User;

public class PhotoController implements ChangeScene {
	/** photo to be displayed */
	private Photo photo;
	/** user who owns the photo that is being displayed individually */
	private User user;
	/** album that individual photo is in */
	private Album album;
	@FXML
	ImageView imageview;
	@FXML
	Text date, caption, tags;
	/** {@link Button} elements of UI to go to next and previous images in {@link #album} */
	@FXML
	Button previous, next;
	
	/**
	 * Returns back to the album content view
	 * @param evt - event triggered by clicking go back button
	 * @throws ClassNotFoundException
	 */
	@FXML
	private void goBack(ActionEvent evt) throws ClassNotFoundException {
		loadAlbumContent(evt, album, user);
	}
	
	/**
	 * Changes image to next photo in the album
	 * @param evt- event triggered by clicking on next button
	 * @throws ClassNotFoundException
	 */
	@FXML
	private void next(ActionEvent evt) throws ClassNotFoundException {
		int index = album.getIndexOf(photo)+1;
		loadPhoto(evt, user, album, album.getPhoto(index), index);
	}
	
	/**
	 * Changes image to previous photo in the album
	 * @param evt- event triggered by clicking on previous button
	 * @throws ClassNotFoundException
	 */
	@FXML
	private void previous(ActionEvent evt) throws ClassNotFoundException {
		int index = album.getIndexOf(photo)-1;
		loadPhoto(evt, user, album, album.getPhoto(index), index);
	}
	
	/**
	 * Loads the edit tags page to edit the tags
	 * @param evt - event triggered by clicking on edit tags button
	 * @throws ClassNotFoundException
	 */
	@FXML
	private void editTags(ActionEvent evt) throws ClassNotFoundException {
		loadEditTags(evt, user, album, photo);
	}
	
	/**
	 * Loads the edit caption page to edit the caption
	 * @param evt - event triggered by clicking on edit caption button
	 * @throws ClassNotFoundException
	 */
	@FXML
	private void editCaption(ActionEvent evt) throws ClassNotFoundException {
		loadEditCaption(evt, user, album, photo);
	}
	
	/**
	 * Start function to initialize necessary variables.
	 * @param user - current {@link User}
	 * @param album - current {@link Album}
	 * @param photo - current {@link Photo}
	 */
	public void start(User user, Album album, Photo photo, int index) {
		this.user = user;
		this.album = album;
		this.photo = photo;
		imageview.setImage(photo.getImage());
		date.setText(Photo.convertDate(photo.getDate()));
		caption.setText(photo.getCaption());
		// disable previous button if first photo
		if (album.getIndexOf(photo) == 0) {
			previous.setDisable(true);
		}
		// disable next button if last photo
		if (album.getIndexOf(photo) == album.size()-1) {
			next.setDisable(true);
		}
		StringBuffer sb = new StringBuffer();
		ArrayList<Tag> list = photo.getTags();
		for (Tag t : list) {
			String s = list.indexOf(t) == list.size()-1 ? t.getKey()+" : "+t.getValue() : t.getKey()+" : "+t.getValue()+", ";
			sb.append(s);
		}
		tags.setText(sb.toString()+"");
	}
}
