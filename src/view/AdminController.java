package view;

import java.util.ArrayList;
import java.util.Optional;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Callback;
import model.Database;
import model.User;

/**
 * Controller for admin home and new user. Handles creation and deletion of users.
 * @author Kunal Pednekar(ksp101)
 *
 */
public class AdminController implements ChangeScene {
	/** {@link ListView} that displays all users */
	@FXML
	ListView<User> listView;                
	/** {@link Button} elements in UI for admin home screen */
	@FXML
	Button createUser, deleteUser, logout;
	/** {@link TextField} elements in UI for admin home screen */
	@FXML
	TextField name, username;
	/** {@link Text} elements in UI for admin home screen */
	@FXML
	Text nameError, usernameError;
	/** {@link ObservableList} that represents all non-admin {@link User}s */
	private ObservableList<User> obsList;              

	/**
	 * Reverts back from add new user page
	 * @param evt - event triggered by click on cancel button in add new user page
	 * @throws ClassNotFoundException
	 */
	@FXML
	private void cancel(ActionEvent evt) throws ClassNotFoundException {
		loadAdminHome(evt);
	}
	
	/**
	 * Validates the input values for new user. Sets error message accordingly.
	 * If validation succeeds, makes call to {@link model.Database#addUser(User)}
	 * @param evt - event triggered by click on done button in add new user page
	 * @throws ClassNotFoundException
	 */
	@FXML
	private void validateNewUser(ActionEvent evt) throws ClassNotFoundException {
		String uid = username.getText();
		String uname = name.getText();
		boolean errors = false;
		
		final String nameEmptyErrorMsg = "Name cannot be empty";
		if (uname.trim().equals("")) {
			nameError.setText(nameEmptyErrorMsg);
			errors = true;
		} else {
			nameError.setText("");
		}
		
		final String usernameEmptyErrorMsg = "Username cannot be empty";
		if (uid.trim().equals("")) {
			usernameError.setText(usernameEmptyErrorMsg);
			errors = true;
		} else {
			usernameError.setText("");
		}
		
		if (Database.getUser(uid) == null && !errors) {
			User user = new User(uname, uid);
			Database.addUser(user);
			// incomplete
			loadAdminHome(evt);
		} else if (Database.getUser(uid) != null && !errors) {
			final String usernameTakenErrorMsg = "Username already taken";
			usernameError.setText(usernameTakenErrorMsg);
			errors = true;
		}
	}
	
	/**
	 * Delete a user from the application. Displays alert box upon error
	 * @param evt - event triggered by click on delete button in admin home page
	 */
	@FXML
	private void handleDelete(ActionEvent evt) {
		// check if ArrayList of users is already empty
		if (obsList.isEmpty()) {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Warning Dialog");
			alert.setHeaderText("Cannot delete user");
			alert.setContentText("List is empty or no user selected");
			alert.showAndWait();
			return;
		}
		User u = listView.getSelectionModel().getSelectedItem();
		int index = listView.getSelectionModel().getSelectedIndex();
		if (obsList.contains(u)) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Delete User");
			alert.setHeaderText("Press OK to confirm or the cross to cancel");

			String content = "Are you sure you want to delete " + u.getName() + "?";

			alert.setContentText(content);

			Optional<ButtonType> result = alert.showAndWait();
			if (result.isPresent()) {
				obsList.remove(u);
				Database.deleteUser(u);
				if(index == obsList.size()-1)
					listView.getSelectionModel().select(index--);
				else
					listView.getSelectionModel().select(index++);
				listView.setItems(obsList);
			}		
		}
	}
	
	/**
	 * Logout function for admin user.
	 * @param evt - ActionEvent triggered by mouse click on logout button
	 * @throws ClassNotFoundException
	 */
	@FXML
	private void handleLogout(ActionEvent evt) throws ClassNotFoundException {
		System.out.println("bye");
		logout(evt);
	}
	
	/**
	 * Adds a user to the user list.
	 * @param evt - event triggered by click on add user button in admin home page
	 * @throws ClassNotFoundException
	 */
	@FXML
	private void addUser(ActionEvent evt) throws ClassNotFoundException {
		loadNewUser(evt);
	}
	
	/**
	 * Start method for the controller
	 * @param mainStage - stage that this scene will be displayed on
	 */
	public void start(Stage mainStage) {
		ArrayList<User> list = Database.getUsers();
		obsList = FXCollections.observableArrayList(list);               
		listView.setItems(obsList);
		
		listView.setCellFactory(new Callback<ListView<User>, ListCell<User>>(){
			@Override
			public ListCell<User> call(ListView<User> p) {
				ListCell<User> cell = new ListCell<User>(){
					@Override
					protected void updateItem(User u, boolean bln) {
						super.updateItem(u, bln);
						if (u != null) {
							// here we are setting the song name and title that we see in the list
							setText(u.getName()+" ("+u.getUsername()+")");
							  
					            
						}
						else if (u == null) {
							setText(null);
						}
					}
				};
				return cell;
			}
		});
		// preselect first element
		if (!obsList.isEmpty())
			listView.getSelectionModel().select(0);
	}
}
