package view;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import model.Album;
import model.Photo;
import model.User;

public class EditCaptionController implements ChangeScene {
	/** photo to be displayed */
	private Photo photo;
	/** user who owns the photo that is being displayed individually */
	private User user;
	/** album that individual photo is in */
	private Album album;
	/** {@link TextArea} element of UI to allow capturing new caption */
	@FXML
	TextArea caption;
	
	/**
	 * Start function to initialize the edit caption page
	 * @param user - current {@link User}
	 * @param album - current {@link Album}
	 * @param photo - current {@link Photo}
	 */
	public void start(User user, Album album, Photo photo) {
		this.user = user;
		this.album = album;
		this.photo = photo;
		caption.setText(photo.getCaption());
	}
	
	/**
	 * Sets caption to new value provided in {@link #caption}
	 * @param evt - event triggered by click on done button
	 */
	@FXML
	private void done(ActionEvent evt) {
		String newCaption = caption.getText();
		photo.updateCaption(newCaption);
		album.updatePhoto(photo);
		user.updateAlbum(album);
		try {
			loadPhoto(evt, user, album, photo, album.getIndexOf(photo));
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Cancels current action and returns to individual photo view
	 * @param evt - event triggered by click on cancel button
	 */
	@FXML
	private void cancel(ActionEvent evt) {
		try {
			loadPhoto(evt, user, album, photo, album.getIndexOf(photo));
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
